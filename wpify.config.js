module.exports = {
	wordPressUrl: 'https://www.wpify-plugin.test',
	config: {
		build: 'build',
		entry: {
			'plugin': './assets/plugin.js'
		},
	},
	webpack: (config) => {
		return config;
	},
	browserSync: (config) => {
		return config;
	}
};
